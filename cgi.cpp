#include <iostream>
#include <fstream> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>
#include <sys/uio.h>
#include <sys/fcntl.h>
#include <sys/unistd.h>
#include <arpa/inet.h>
#include <errno.h>
using namespace std;

#define Maxline 1000000
#define Midline 256
#define ServerNumber 5

#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

typedef struct reqdata{
	char h1[Midline];
	char h2[Midline];
	char h3[Midline];
	char h4[Midline];
	char h5[Midline];
	char p1[ServerNumber];
	char p2[ServerNumber];
	char p3[ServerNumber];
	char p4[ServerNumber];
	char p5[ServerNumber];
	char f1[Midline];
	char f2[Midline];
	char f3[Midline];
	char f4[Midline];
	char f5[Midline];

}reqdata;

reqdata req;

class Myser {
public:
	char sockhost[255];
	int sockport;
	char host[255];
	int  port;
	char charport[8];
	char filename[255];
	int sock_fd;
	int NeedWrite;
	int statusA ;
	FILE *pFile ;
	int id;
	
	Myser(void);
	void get(char *,  char *,  char *);
};
Myser::Myser(void) {
	memset(host, 0, sizeof(host));
	memset(filename, 0, sizeof(filename));
	port = 0;
}
void Myser::get(char *shost, char* sport,char *sfilename) {
	strcpy(host,shost);
	strcpy(filename,sfilename);
	port = atoi(sport);
}
void print_table_row();
Myser sc[ServerNumber];
int flag[ServerNumber];
int conn = 0; 
void init_req(){
	char ip[256], port[256], file[256];
	char sockhost[256], sockport[256];
	//QUERY_STRING	傳遞給CGI程式的query資訊，也就是用"?"隔開，添加在URL後面的字串。
	char *query = getenv("QUERY_STRING");
	char * token;
	/*	而瀏覽器會以『mane1=value1&name2=value2&...』
		的形式將資料傳送給server， 因此我們取得資料串後的第一步
		就是依"&"這個分隔符號把每個變數分開
		original  ->
		h1=www.nctu.edu.tw&p1=5566&f1=t1.txt
	*/
	
	for (int i = 0; i < ServerNumber; i++)
	{
		flag[i]=0;
		//host	port	file
		if (i == 0)
			token = strtok(query, "&");
		else
			token = strtok(NULL, "&");
		//h1=npbsd1.cs.nctu.edu.tw
		sprintf(ip, "h%d=", i + 1);
		if (!strncmp(token, ip, 3) && strlen(token) > 3)
			strncpy(sc[i].host, (token + 3), strlen(token + 3));
		if(strstr(sc[i].host,"140.113.167.")){
			memset(sc[i].host,0,sizeof(sc[i].host));
			flag[i]=1;
		}
		
		//p1=5566
		sprintf(port, "p%d=", i + 1);
		token = strtok(NULL, "&");
		if (!strncmp(token, port, 3) && strlen(token) > 3)
			strncpy(sc[i].charport, (token + 3), strlen(token + 3));
		//f1=1.txt
		sprintf(file, "f%d=", i + 1);
		token = strtok(NULL, "&");
		if (!strncmp(token, file, 3) && strlen(token) > 3)
			strncpy(sc[i].filename, (token + 3), strlen(token + 3));
		sprintf(sockhost, "sh%d=", i + 1);
		token = strtok(NULL, "&");
		if (!strncmp(token, sockhost, 4) && strlen(token) > 4)
			strncpy(sc[i].sockhost, (token + 4), strlen(token + 4));
		
		sprintf(sockport, "sp%d=", i + 1);
		token = strtok(NULL, "&");
		if (!strncmp(token, sockport, 4) && strlen(token) > 4)
			sc[i].sockport = atoi(token + 4);
	}
}
void init(){

			for(int i =0 ;i<ServerNumber ;i++){
				sc[i].port = atoi(sc[i].charport);
			}

}
int request(int ssock, int port, unsigned int ip)
{
	unsigned char package[8];
	
	package[0] = 4;
	package[1] = 1 ; // CD
	package[2] = port / 256;
	package[3] = port % 256;
	package[4] = ip >> 24;
	package[5] = (ip >> 16) & 0xFF;
	package[6] = (ip >> 8)  & 0xFF;
	package[7] = ip & 0xFF;
	
	write(ssock, package, 8);
}

void begin_html(){
	/*
		<html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=big5" />
        <title>Network Programming Homework 3</title>
        </head>
        <body bgcolor=#336699>
        <font face="Courier New" size=2 color=#FFFF99>
        <table width="800" border="1">
	*/
	printf("Content-type: text/html\n");
	printf("\n");
	printf("<html>"
			"<head>"
			"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />"
			"</head>"
			"<body bgcolor=#336699>"
			"<font face=\"Courier New\" size=2 color=#FFFF99>"
					"<table width=\"800\" border=\"1\">");
	print_table_row();
	
}
void print_table_row(){
	/*  
		<tr>
        <td>140.113.210.145</td><td>140.113.210.145</td><td>140.113.210.145</td></tr>
        <tr>
        <td valign="top" id="m0"></td><td valign="top" id="m1"></td><td valign="top" id="m2"></td></tr>
        </table>
	*/
	printf("<tr>");
	for(int i = 0;i<ServerNumber;i++){
		if(strlen(sc[i].host)!=0){
			
			printf("<td>%s</td>",sc[i].host);
			//if(strlen(sc[i].sockhost)!=0)
				//printf("Sock : %s",sc[i].sockhost);
			sc[i].id = conn;
			conn++;
		}
		else if(strlen(sc[i].host)==0&&(flag[i]==1)){
			printf("<td>connection denied </td>");
		}
		else{
			printf("<td>No user </td>");
		}
			
	}
	printf("</tr>");
	//cout<<conn<<endl;

	printf("<tr>");
	
	for(int i = 0;i<ServerNumber;i++){
		printf("<td valign=\"top\" id=\"m%d\"></td>",i);
	}
	
	printf("</tr>");
	printf("</table>");

}
void end_html(){
	
	
	printf("</font>"
		"</body>"
		"</html>");
	
}
int readline(int fd,char* ptr,int maxlen)
{
	int n,rc;
	char c;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;
			if(c=='\n'){
				break;
			}else if(c=='%'){
				rc=read(fd,&c,1);
				break;			
			} 
		}
		else if(rc==0)
		{
			if(n==1) return 0;
			else break;
		}
		else return -1;
	}
	*ptr=0;
	return n;
}
void errexit(){
	cout<<"error<br>";
	
	exit(1);

}
int start(){

	int nfds = FD_SETSIZE;
	
	
	int fd_max=0;
	fd_set read_fd;
	fd_set tmp_fd;
	FD_ZERO(&read_fd);
    FD_ZERO(&tmp_fd);

	for(int i=0,j=0; i<ServerNumber;i++){

		if((j=strlen(sc[i].host))!=0){
			
			int    client_fd;
			struct sockaddr_in client_sin;
			struct hostent *he; 

			int   flags ;
			
			client_fd = socket(AF_INET,SOCK_STREAM,0);
			if(sc[i].sockport==0)
				he=gethostbyname(sc[i].host);
			else
				he=gethostbyname(sc[i].sockhost);
			bzero(&client_sin,sizeof(client_sin));
			client_sin.sin_family = AF_INET;
			client_sin.sin_addr = *((struct in_addr *)he->h_addr); 
			if(sc[i].sockport==0)
				client_sin.sin_port = htons(sc[i].port);
			else
				client_sin.sin_port = htons(sc[i].sockport);
			flags = fcntl(client_fd, F_GETFL, 0);
			flags |= O_NONBLOCK;
			
			if(fcntl(client_fd, F_SETFL, flags) <0){
				cout <<"fcntl erro<br>";
				exit(-1);
			}
			
			if(connect(client_fd,(struct sockaddr *)&client_sin,sizeof(client_sin)) == -1)
			{
				if (errno != EINPROGRESS){			
					cout<<errno<<" :erro when connecting <br>";
					fflush(stdout);
					exit(1);
				}
			}
			sc[i].sock_fd = client_fd;
			if(strlen (sc[i].sockhost) != 0){
				
				struct hostent *tmp ;
				tmp = gethostbyname(sc[i].host);
				unsigned int ip = (unsigned int)ntohl(inet_addr(inet_ntoa(*((struct in_addr *)tmp->h_addr))));
				unsigned char buffer[8];
				request(sc[i].sock_fd,sc[i].port, ip);
				
				while(1){
					int result;
					result = read(sc[i].sock_fd,buffer, 8);
					if(result >= 0)
						break;
					sleep(2);
					request(sc[i].sock_fd,sc[i].port, ip);
				}
				unsigned char CD=buffer[1];
				if(CD==91){
					conn--;
					continue;					
				}
			}			
			sc[i].pFile = fopen (sc[i].filename , "r");
			FD_SET(client_fd, &read_fd);
			fd_max = client_fd > fd_max ? client_fd : fd_max;
	   }
	}
	while(conn>0)
	{
		tmp_fd = read_fd;
		if(select(fd_max+1, &tmp_fd, NULL, NULL, NULL) <0){
			cout<<"Select error.<br>" << fflush;
			exit(1);				
		}	
        for(int i=0;i<fd_max+1; i++){
			if(FD_ISSET(i, &tmp_fd))
			{
				for (int x=0; x< ServerNumber  ; x++)
				{
					if(i == sc[x].sock_fd)
					{
						char send_msg[Maxline]={0};
						char msg[Maxline]={0};	
						char read_buf[Maxline] ={0};
						int len = readline(sc[x].sock_fd, read_buf, sizeof(read_buf));
						//for(int i=0;i<5;i++){
						//}
						if(len <0){
							cout<<"Error: Read from socket error.\n" << fflush;
							conn--;
							exit(1);
						}
						else if(len == 0){
							conn--;
							shutdown(sc[x].sock_fd, SHUT_RDWR);
							FD_CLR(sc[x].sock_fd, &read_fd);
							break;
						} 
						else if(strncmp(read_buf, "%", strlen("%"))==0){
							//transfer ins to server
							/*
								<script>document.all['m1'].innerHTML += "<br>";</script>
								<script>document.all['m1'].innerHTML += "Test<br>";</script>
								<script>document.all['m1'].innerHTML += "This is a test program<br>";</script>
							*/
							printf ("<script>document.all['m%d'].innerHTML += \"%s \";</script>",x,read_buf);
							fflush(stdout);
							bzero(read_buf, sizeof(read_buf));
							/*	
								fgets() 從檔案一行一行的讀取資料，共需三個參數，
								第一個參數為儲存輸入資料的陣列，第二個參數為該行最多幾個字元
								，第三個參數為指向結構 FILE 的指標。
							*/
							fgets(send_msg, sizeof(send_msg), sc[x].pFile);
							strcpy(msg ,send_msg);
							int j=1;
							for(int k=1;k<=strlen(msg);k++,j++)
							{
								if(msg[k]==' '&&msg[k-1]==' ')
								{
									j--;
								}
								msg[j]=msg[k];
							}
							//msg[j] = '\n';
							write(sc[x].sock_fd, send_msg, strlen(send_msg));                      
						
							char* mystr = strtok(send_msg,"\n\r\t");

							printf ("<script>document.all['m%d'].innerHTML += \"%s<br>\";</script>",x,mystr);
							fflush(stdout);
							if(strcmp(send_msg,"exit") ==0 ){
								shutdown(sc[x].sock_fd, SHUT_RDWR);
								FD_CLR(sc[x].sock_fd, &read_fd);
								conn--;
							}
						}else{
							//print result to client
							char* mystr = strtok(read_buf,"\n\r\t");
							fflush(stdout);
							printf ("<script>document.all['m%d'].innerHTML += \"<b>%s</b><br>\";</script>",x,read_buf);
							fflush(stdout);
							bzero(read_buf, sizeof(read_buf));
						}
					}
				}
			}
		}
    }
}
int main(int argc, char* argv[], char *envp[]) {

	init_req();

	int flag ;
	flag = fcntl(0, F_GETFL, 0);
	fcntl(0, F_SETFL, flag|O_NONBLOCK);

	char tmp[1024];
	read(0,tmp,1024);
	flag = fcntl(1, F_GETFL, 0);
	fcntl(1, F_SETFL, flag|O_NONBLOCK);

	fflush(stdout);
	
	init();
	begin_html();
	end_html();

	start();
	
	return 0;
}
