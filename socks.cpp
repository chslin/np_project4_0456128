#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include<arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <errno.h>
#include <stdarg.h>
#include <sys/uio.h>
#include <fcntl.h>

#define CONNECT		1
#define BIND		2

#define MAXBUFF 5000
#define MAXLINE 1000

using namespace std;
int dealRequest(int clientfd, struct sockaddr_in client_sin);
void serverMsg(unsigned char request[], struct sockaddr_in client_sin, bool permit);
void sendReply(int clientfd, unsigned char request[], int mode, bool permit);
int recv_msg(int from, int des);
void redirectTrans(int clientfd, int ssockfd);
void openSocksConf();
unsigned char gPermitIP[2] = {0};
int bind_port = 64320;
int main() {
	int sockfd;
	struct sockaddr_in server_addr;

	/* create socket , same as client */
	if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
		;//err_dump("Server: can't open stream socket");

	/* initialize structure server_addr */
	bzero(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(64200);
	/* this line is different from client */
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	int isSetSockOk = 1;
	setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&isSetSockOk,sizeof(int));

	/* Assign a port number to socket */
	if ( bind(sockfd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0 )
		perror("Server: can't bind local address");
	/* make it listen to socket with max 100 connections */
	listen(sockfd, 100);
		// =========================== Open & Read Socks.conf =========================
	

	/* infinity loop -- accepting connection from client forever */
	while (1) {
		
		int clientfd;
		int childpid;
		struct sockaddr_in client_addr;
		int addrlen = sizeof(client_addr);

		/* Wait and Accept connection */
		clientfd = accept(sockfd, (struct sockaddr*) &client_addr, (socklen_t *) &addrlen);
		bind_port++;
		if ( clientfd < 0 )
			;//err_dump("Server: accept error");
		if ( (childpid = fork()) < 0 )
			;//err_dump("Server: fork error");
		else if ( childpid == 0 ) { // Child process
			// close original socket
			close(sockfd);

			//printf("%d	Client Connected...\n", clientfd);
			// process the request
			openSocksConf();
			dealRequest(clientfd, client_addr);
			exit(0);
		}

		/* close(client) */
		close(clientfd);
	}
	/* close(server) , but never get here because of the loop */
	//close(sockfd);
	return 0;
}
int dealRequest(int clientfd, struct sockaddr_in clientSOCKS_sin)
{
	int n;
	bool permitAccess = true; // Assume!!!
	unsigned char request[100];

	struct sockaddr_in client_sin;
	struct hostent *he;
	int ssockfd;

	int sockftpfd;
	struct sockaddr_in server_addr;
	int serverftpfd;
	struct sockaddr_in client_addr;
	int addrlen = sizeof(client_addr);
	int isSetSockOk = 1;

	char servip[30] = {'\0'};
	int servport = 0;

	n = read(clientfd, request, 100);
	if ( n == 0 )
		return -1; // connection terminated
	else if ( n < 0 )
		;//err_dump("str_echo: readline error");
	else {
		// ================================== Check FireWall RuleSet ================================
		if ( (request[4] == gPermitIP[0] && request[5] == gPermitIP[1]) || (gPermitIP[0] == 0 && gPermitIP[1] == 0) )
			permitAccess = true;
		else
			permitAccess = false;
		// ==========================================================================================

		serverMsg(request, clientSOCKS_sin, permitAccess);
		// Send Reply
		sendReply(clientfd, request, request[1], permitAccess);
		if ( permitAccess ) {
			if ( request[1] == CONNECT ) {
				//printf("============ CONNECT ============\n");
				// Get DestServer PORT & IP
				servport = request[2]*256 + request[3];
				sprintf(servip, "%d.%d.%d.%d", request[4], request[5], request[6], request[7]);

				if ( ( he = gethostbyname(servip) ) == NULL ) {
				    printf("Usage : client <server ip> error...\n");
				} else {
					ssockfd = socket(AF_INET,SOCK_STREAM,0);
				    bzero(&client_sin,sizeof(client_sin));
				    client_sin.sin_family = AF_INET;
				    client_sin.sin_addr = *((struct in_addr *)he->h_addr);
				    client_sin.sin_port = htons(servport);
				}
				isSetSockOk = 1;
				setsockopt(sockftpfd,SOL_SOCKET,SO_REUSEADDR,&isSetSockOk,sizeof(int));

				if( connect(ssockfd,(struct sockaddr *)&client_sin,sizeof(client_sin)) == -1 ) {
					printf("Connect Error...\n");
					exit(1);
				} else ;

				 //sleep(1);     //waiting for welcome messages

				 // Redirect transport data
				 redirectTrans(clientfd, ssockfd);

			} else if ( request[1] == BIND ) {
				//printf("============ BIND ============\n");
				if ( (sockftpfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
					;//err_dump("Server: can't open stream socket");

				/* initialize structure server_addr */
				bzero(&server_addr, sizeof(server_addr));
				server_addr.sin_family = AF_INET;
				server_addr.sin_port = htons(bind_port);
				/* this line is different from client */
				server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

				isSetSockOk = 1;
				setsockopt(sockftpfd,SOL_SOCKET,SO_REUSEADDR,&isSetSockOk,sizeof(int));

				/* Assign a port number to socket */
				if ( bind(sockftpfd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0 )
					perror("Server: can't bind local address");
				/* make it listen to socket with max 100 connections */
				listen(sockftpfd, 10);

				/* Wait and Accept connection */
				serverftpfd = accept(sockftpfd, (struct sockaddr*) &client_addr, (socklen_t *) &addrlen);

				if ( serverftpfd < 0 )
					printf("FTP Server Connect Error...\n");
				else {
					//printf("============= Accept ==========\n");
					close(sockftpfd);
					// Send Reply
					sendReply(clientfd, request, BIND, permitAccess);
					// Redirect transport data
					redirectTrans(clientfd, serverftpfd);
				}
			} else
				printf("Command Error...\n");
		} else ;
	}

	return 0;
}
void openSocksConf()
{
	FILE *fp;
	char *str;
	char buff[MAXBUFF] = {'\0'};

	if ((fp = fopen("socks.conf", "rt")) == NULL)
		perror("can't open file");
	fread(buff, sizeof(char), sizeof(buff), fp);
	str = strtok(buff, " ");
	for (int i=1; i < 6 && str != NULL ;i++) {
		str = strtok(NULL, ". ");
		if ( i == 2 ) {
			gPermitIP[0] = atoi(str);
		} if ( i == 3 ) {
			gPermitIP[1] = atoi(str);
		}
	}
	printf("==== %d.%d\n",gPermitIP[0],gPermitIP[1]);
	fclose(fp);
}
void serverMsg(unsigned char request[], struct sockaddr_in client_sin, bool permit)
{
	int i;
	int servport = request[2]*256 + request[3];
	char servip[30] = {'\0'};
	char userid[50] = {'\0'};
	for (i=8; request[i] != NULL ;i++)
		userid[i-8] = (char)request[i];
	userid[i] = '\0';
	sprintf(servip, "%d.%d.%d.%d", request[4], request[5], request[6], request[7]);
	printf("VN: %d, CD: %d, DST IP: %s, DST PORT: %d, USERID: %s\n", request[0], request[1], servip, servport, userid);
	/*for (int i=8; request[i] != NULL ;i++) {
		userid[i-8] = (char)request[i];
	}*/
	if ( permit ) {
		printf("Permit Src = %s(%d), Dst = %s(%d)\n", inet_ntoa(client_sin.sin_addr), client_sin.sin_port, servip, servport);

		if ( request[1] == CONNECT )
			printf("SOCKS_CONNECT ");
		else
			printf("SOCKS_BIND ");
		printf("Accept ....\n");
	} else {
		printf("Deny Src = %s(%d), Dst = %s(%d)\n", inet_ntoa(client_sin.sin_addr), client_sin.sin_port, servip, servport);

		if ( request[1] == CONNECT )
			printf("SOCKS_CONNECT ");
		else
			printf("SOCKS_BIND ");
		printf("Reject ....\n");
	}
}

void sendReply(int clientfd, unsigned char request[], int mode, bool permit)
{
	unsigned char reply[8];
	reply[0] = 0;
	if ( permit )
		reply[1] = 90;
	else
		reply[1] = 91;
	if ( mode == CONNECT ) {
		for (int i=2; i < 8 ;i++)
			reply[i] = request[i];
	} else {
		int port = bind_port;

		reply[2] = port / 256;
		reply[3] = port % 256;
		for (int i=4; i < 8 ;i++)
			reply[i] = 0;
	}

	send(clientfd, reply, 8, 0);
}

int recv_msg(int from, int des)
{
	char buf[10000];
	int len;
	if((len=read(from,buf,sizeof(buf)-1)) <0) return -1;
	buf[len] = 0;
	//printf("%s\n",buf);	//echo input
	//fflush(stdout);
	if ( len > 0 )
		send(des, buf, len, 0);

	return len;
}

void redirectTrans(int clientfd, int ssockfd)
{
	fd_set readfds;
	while (1) {
		FD_ZERO(&readfds);
		FD_SET(clientfd, &readfds);
		FD_SET(ssockfd, &readfds);

		if (select(1024, &readfds, NULL, (fd_set*) 0, (struct timeval*) 0) < 0)
			exit(1);

		if (FD_ISSET(clientfd, &readfds)) {
			//receive clientfd message
			//printf("clientfd -> ssockfd\n");
			if (recv_msg(clientfd, ssockfd) <= 0) {
				printf("Read end...\n");
				shutdown(clientfd, 2);
				close( clientfd );
				exit(1);
			}
		}

		if (FD_ISSET(ssockfd, &readfds)) {
			//receive ssockfd message
			//printf("ssockfd -> clientfd\n");
			if (recv_msg(ssockfd, clientfd) <= 0) {
				printf("Read end...\n");
				shutdown(ssockfd, 2);
				close( ssockfd );
				exit(1);
			}
		}
	} // while(1) end
}
